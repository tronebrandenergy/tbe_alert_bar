<?php

namespace Drupal\tbe_alert_bar\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "tbe_alert_bar_block",
 *   admin_label = @Translation("Alert Block"),
 * )
 */
class AlertBarBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {
        $config = \Drupal::config('tbe_alert_bar.settings');
        $message = $config->get('message');
        $box_color = $config->get('box_color');
        $box_custom_color = $config->get('box_custom_color');
        $icon = $config->get('icon');
        $icon_color = $config->get('icon_color');
        $icon_custom_color = $config->get('icon_custom_color');
        $active = $config->get('active');
        $expiration = $config->get('expiration');

        // Render array
        $output = [];

        // Only displays Alert Block if active
        if ($active) {
            // Load alert icon
            $term = Term::load($icon);
            $alertIcon = NULL;
            if (!empty($term)) {
                $alertIcon = $term->get('field_fa_alert_icon')->value;
            }

            $output = [
                '#cache' => ['max-age' => 0],
                '#theme' => 'tbe_alert_bar__block',
                '#message' => $message,
                '#icon' => $alertIcon,
                '#box_color' => $box_color,
                '#box_custom_color' => $box_custom_color,
                '#icon_color' => $icon_color,
                '#icon_custom_color' => $icon_custom_color,
                '#attached' => [
                    'library' => [
                        'tbe_alert_bar/general',
                    ],
                    'drupalSettings' => [
                        'alertBoxSettings' => [
                            'expiration' => $expiration
                        ]
                    ]
                ],
            ];
        }

        return $output;
    }

}
