<?php

namespace Drupal\tbe_alert_bar\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class AlertConfigForm extends ConfigFormBase {

  const CONFIG_NAME = 'tbe_alert_bar.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alert_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * Returns this modules configuration object.
   */
  protected function getConfig() {
    return $this->config(self::CONFIG_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfig();
    $body = $config->get('message');

    $form['expiration'] = [
      '#type' => 'select',
      '#title' => $this->t('Cookie expiration time'),
      '#options' => [
        '1/48' => '30 Minutes',
        '2/48' => '1 Hour',
        '0.5' => '12 Hours',
        1 => '1 Day',
        7 => '1 Week',
      ],
      '#default_value' => $config->get('expiration') ?? 1,
      '#required' => TRUE
    ];

    $form['framework'] = [
      '#type' => 'select',
      '#title' => $this->t('Theme framework'),
      '#options' => [
        'bootstrap'=>'Bootstrap',
        'foundation'=>'Foundation'
      ],
      '#empty_value' => '',
      '#empty_option' => '-- Select a Framework --',
      '#default_value' => $config->get('framework'),
      '#required' => TRUE
    ];

    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active/Inactive'),
      '#default_value' => $config->get('active'),
      '#description' => $this->t('This will activate or deactivate the alert bar on the website')
    ];

    $icons = [];
    $vid = 'tbe_alert_bar_icon';
    $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $icons[$term->tid] = $term->name;
    }

    $form['icon_settings'] = [
      '#type' => 'details',
      '#title' => 'Icon Settings',
      '#open' => TRUE,
    ];

    $form['icon_settings']['icon'] = [
      '#type' => 'select',
      '#title' => $this->t('Icon for alert'),
      '#options' => $icons,
      '#default_value' => $config->get('icon'),
    ];

    $form['icon_settings']['icon_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Icon color'),
      '#options' => [
        'yellow' => 'Yellow',
        'white' => 'White',
        'pink' => 'Pink',
        'Gray' => 'Gray',
        'red' => 'Red',
        'purple' => 'Purple',
        'black' => 'Black',
        'custom' => 'Custom'
      ],
      '#empty_value' => '',
      '#empty_option' => '-- Select a color --',
      '#attributes' => [
        'id' => 'icon_color_selector',
      ],
      '#default_value' => $config->get('icon_color'),
      '#required' => TRUE
    ];

    $form['icon_settings']['icon_custom_color'] = [
      '#type' => 'textfield',
      '#size' => '60',
      '#title' => $this->t('Custom color'),
      '#default_value' => $config->get('icon_custom_color'),
      '#description' => 'Color format should be a HEX value (#FFFFFF)',
      '#states' => [
        'visible' => [
          ':input[id="icon_color_selector"]' => [
            'value' => 'custom'
          ],
        ],
        'required' => [
          ':input[id="icon_color_selector"]' => [
            'value' => 'custom'
          ],
        ],
      ]
    ];

    $form['message_settings'] = [
      '#type' => 'details',
      '#title' => 'Message Settings',
      '#open' => TRUE,
    ];

    $form['message_settings']['box_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Background color'),
      '#options' => [
        'yellow' => 'Yellow',
        'white' => 'White',
        'pink' => 'Pink',
        'Gray' => 'Gray',
        'red' => 'Red',
        'purple' => 'Purple',
        'black' => 'Black',
        'custom' => 'Custom'
      ],
      '#empty_value' => '',
      '#empty_option' => '-- Select a color --',
      '#attributes' => [
        'id' => 'box_color_selector',
      ],
      '#default_value' => $config->get('box_color'),
      '#required' => TRUE
    ];

    $form['message_settings']['box_custom_color'] = [
      '#type' => 'textfield',
      '#size' => '60',
      '#title' => $this->t('Custom color'),
      '#default_value' => $config->get('box_custom_color'),
      '#description' => 'Color format should be a HEX value (#FFFFFF)',
      '#states' => [
        'visible' => [
          ':input[id="box_color_selector"]' => [
            'value' => 'custom'
          ],
        ],
        'required' => [
          ':input[id="box_color_selector"]' => [
            'value' => 'custom'
          ],
        ],
      ]
    ];

    $form['message_settings']['message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message'),
      '#default_value' => isset($body['value']) ? $body['value'] : '',
      '#format' => isset($body['format']) ? $body['format'] : 'full_html',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfig();
    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();

    $this->messenger()->addMessage('Configuration changes were successfully saved.');
  }

}
