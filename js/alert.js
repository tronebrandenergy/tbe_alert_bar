(function(Drupal, drupalSettings, $) {
    Drupal.behaviors.tbe_alert_bar = {
        attach: function(context, settings) {
            // Cookie for alert banner
            if (Cookies.get('alertdismissed') == null) {
                $('#block-alert').slideDown();
            } 
            else {
                $('#block-alert').removeClass().addClass('has-cookie');
            }
            $('#block-alert button.dismiss').click(function (e) {
                $('#block-alert').slideUp();
                var expiration = drupalSettings.alertBoxSettings.expiration;
                var expiredTime = typeof expiration == 'string' || typeof expiration == 'number' ? eval(drupalSettings.alertBoxSettings.expiration) : 1;
                Cookies.set('alertdismissed', 'true', { expires: expiredTime });
            });
        }
    }

    Drupal.behaviors.tbe_countdown = {
        attach: function(context, settings) {
            once('initCountdown', '[data-countdown]').forEach(function(elem, index) {
                window.countdowns = window.countdowns || [];
                var date = $(elem).data('countdown');
                var countDownDate = new Date(date).getTime();
                $(elem).attr('data-countdown-position', index);
                var now = new Date().getTime();
                var timeleft = countDownDate - now;
                $(elem).html(renderTimer(timeleft));

                window.countdowns.push(setInterval(function() {
                    var _now = new Date().getTime();
                    var _timeleft = countDownDate - _now;

                    $(elem).html(renderTimer(_timeleft));

                    // Display the message when countdown is over
                    if (_timeleft < 0) {
                        clearInterval(window.countdowns[index]);
                        $(elem).html('0s');
                    }
                }, 1000))
            });
        }
    }

    function renderTimer(timeleft) {
        // Calculating the days, hours, minutes and seconds left
        var days = Math.floor(timeleft / (1000 * 60 * 60 * 24));
        days = days >= 0 ? days + 'd ' : '';

        var hours = Math.floor((timeleft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        hours = hours >= 0 ? hours + 'h ' : '';

        var minutes = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60));
        minutes = minutes >= 0 ? minutes + 'm ' : '';

        var seconds = Math.floor((timeleft % (1000 * 60)) / 1000);
        seconds = seconds >= 0 ? seconds + 's ' : '';

        return days + hours + minutes + seconds;
    }
})(Drupal, drupalSettings, jQuery);
